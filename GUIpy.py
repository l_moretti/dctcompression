from PyQt5.QtWidgets import (QApplication, QComboBox, QDialog,
QDialogButtonBox, QFormLayout, QGridLayout, QGroupBox, QHBoxLayout,
QLabel, QLineEdit, QMenu, QMenuBar, QPushButton, QSpinBox, QTextEdit,QSizePolicy,
QVBoxLayout,QFileDialog,QMessageBox,QSlider)
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QIcon, QPixmap,QImage
from PIL import Image

import numpy as np

from scipy.fftpack.realtransforms import dct
from scipy.fftpack.realtransforms import idct
from PyQt5.QtCore import QCoreApplication, Qt
import sys
from matplotlib import pyplot as plt 

status = 0

class Dialog(QDialog):
    
    

    def __init__(self):

        global status
        # inizializzo la finestra
        super(Dialog, self).__init__()
        self.createFormGroupBox()

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
       
        self.setLayout(mainLayout)
        
        #imposto il titolo
        self.setWindowTitle("My Dct Compression")
        #imposto la grandezza iniziale della finestra
        self.setGeometry(800,800,600,600)
        self.showFullScreen()
        

        
    def createFormGroupBox(self):
        self.formGroupBox = QGroupBox("DCT")

        # creo il layout
        layout = QFormLayout()
        
        # aggiungo il bottone dei file
        self.button = QPushButton('Select file')
        self.button.clicked.connect(self.openFile)

        #aggiungo la textbox per il path
       # layout.addRow(QLabel("Select filename:"), )
        self.labelFilename=QLineEdit()

        layout.addRow(self.button, self.labelFilename)
        self.labelDimension = QLabel()
        layout.addRow(QLabel("dimension:"), self.labelDimension)

        # creo la label per il parametro F
        self.labelF=QLineEdit()

        layout.addRow(QLabel("Dimensione Finestra:"), self.labelF)

        # creo la label per il parametro d

        self.labelD=QLineEdit()

        layout.addRow(QLabel("Fattore di Compressione:"), self.labelD)
        #self.labelD.textChanged.connect(self.setSlider)

        #self.slider
        self.slider = QSlider(Qt.Horizontal)
        #self.slider.setFocusPolicy(Qt.StrongFocus)
        #self.slider.setTickPosition(QSlider.TicksBothSides)
        #self.slider.setTickInterval(20)
        #self.slider.setSingleStep(20)
        layout.addRow(QLabel("Slider:"), self.slider)
        self.slider.valueChanged.connect(self.update_compression_rate)

        #self.slider.sliderReleased.connect(self.filterImage)
        self.labelD.editingFinished.connect(self.filterImage)

        #label per errori
        self.labelErrore = QLabel()
        self.labelErrore.setStyleSheet('color: red')
        self.labelErrore.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.labelErrore.setAlignment(Qt.AlignCenter)
        layout.addRow(self.labelErrore)

        # creo il bottone per computare l'immagine
        self.buttonConfirm = QPushButton('Apply')
        self.buttonConfirm.clicked.connect(self.filterImage)

        layout.addRow(self.buttonConfirm)

        self.labelImg1=QLabel()
        self.labelImg1.setGeometry(100,100,100,100)
        self.labelImg1.setStyleSheet('color: yellow')

      

        self.labelImg2=QLabel()
        self.labelImg2.setGeometry(100,100,100,100)
        self.labelImg2.setStyleSheet('color: yellow')

        layout.addRow(self.labelImg1,self.labelImg2)

        self.formGroupBox.setLayout(layout)
    
    
    def setSlider(self):
        global status
        text_compression = self.labelD.text()
        if text_compression != None and text_compression!='':
            compression_rate = int(text_compression)
            finestra = self.labelF.text()           
            if status != compression_rate and finestra != None and finestra!='':
                status = compression_rate
                windows_size = int(self.labelF.text())
                sliderValue = compression_rate*50/windows_size
                print(round(sliderValue))
                self.slider.setValue(round(sliderValue))

    def update_compression_rate(self):
        size = self.slider.value()
        windows_size = self.labelF.text()
        if(windows_size != None and windows_size!=''):
            max_value = 2*int(windows_size)-2
            compression_rate = round(max_value/100*size)
            self.labelD.setText(str(compression_rate))
        

    def openFile(self):
        # creo la dialog per la scelta del file
        filename,_ = QFileDialog.getOpenFileName(self, "Open File", "", "Bitmap (*.bmp)")    
        self.labelFilename.setText(filename)
        storedImage = Image.open(filename)
        self.labelErrore.setText("Elaborazione in corso...")
       # QApplication.exec_ ()
        self.repaint()
        #converto l'immagine in array e converto in scala di grigi
        #arrayImm = np.array(storedImage)

        greyScale = storedImage.convert('L')
        arrayImm = np.array(greyScale)

        #Ottengo le dimensioni dell'immagine
        [nrow, ncol] = arrayImm.shape
        self.labelDimension.setText(str(ncol) + "x" + str(nrow))

        pixmap = QPixmap(filename) 
        winSizeMax = (self.width()-50)/2
        if(pixmap.width() > winSizeMax ):
            pixmap=pixmap.scaledToWidth(winSizeMax)

        self.labelImg1.setPixmap(pixmap)     
       # img = cv2.imread(filename,0) 
        #array=GetArrayHistogram(arrayImm)
        #zeros = np.zeros(256)
        plt.figure(1)
        plt.hist(np.array(arrayImm).flatten(),256,[0,256])

        #plt.fill_between(array,zeros, color="blue", alpha=0.3)
        plt.show()
        self.labelErrore.setText("")

    def filterImage(self):   
        #ottengo i valori delle input
        filename = self.labelFilename.text()
        d= self.labelD.text()
        f= self.labelF.text()
         
        #controllo i valori inseriti
        if( d=="" or f=="" or filename=="" or 2* int(f)-2 < int(d) ):
            self.labelErrore.setText("Controlla i campi inseriti")
            return None
        self.labelErrore.setText("")
        #carico l'immagine
        storedImage = Image.open(filename)
        #storedImage.show()

        #converto l'immagine in array e converto in scala di grigi
        #arrayImm = np.array(storedImage)
        self.labelErrore.setText("Elaborazione in corso...")
        #QApplication.exec_ ()
        self.repaint()
        greyScale = storedImage.convert('L')
        arrayImm = np.array(greyScale)

        #Ottengo le dimensioni dell'immagine
        [nrow, ncol] = arrayImm.shape

        f = int(f) 

        # calcolo quante finestre userò
        numColonneFinestra = int(nrow / f)
        numRigheFinestra = int(ncol / f)
        
        # inizializzo la matrice risultato
       # imageVert = arrayImm
        resultMatrix = arrayImm

        # scorro le finestre
        for cFin in range(0,numColonneFinestra):
            for rFin in range(0,numRigheFinestra):

                #calcolo gli indici di partenza per la finestra
                indiceXIniziale = cFin * int(f)
                indiceYIniziale = rFin * int(f)

                # inizializzo la finestra
                finestraAttualearray=np.zeros((f,f))

                # copio il pezzo di immagine reale nella finestra attuale
                xTemp=0
                yTemp=0
                for rTemp in range(indiceXIniziale,indiceXIniziale + f):
                    yTemp=0
                    for cTemp in range(indiceYIniziale,indiceYIniziale + f): 
                        finestraAttualearray[xTemp,yTemp]=arrayImm[rTemp,cTemp]
                        yTemp= yTemp + 1
                    xTemp= xTemp + 1    

                # applico la DCT all'intera immagine
                imageVert = dct(dct(finestraAttualearray.T,norm='ortho').T, norm='ortho')

                # scorro e tolgo le frequenze decise dall'utente
                for f1 in range(0,f):
                                    for f2 in range(0,f):
                                        if f1+f2>= int(d):
                                            imageVert[f1][f2]=0
                # applicazione della IDCT
                invertedVer = idct(idct(imageVert.T, norm='ortho').T, norm='ortho')

                #arrotondamento e sistemazione dei valori dei pixel
                for kkm in range(0,f):
                    for kkn in range(0,f):
                        
                        #Arrotondo tutti i valori all'intero più vicino
                        invertedVer[kkm][kkn]=round(invertedVer[kkm][kkn])

                        #I valori negativi o superiori a 255 vengono portati a o e 255 
                        if invertedVer[kkm][kkn]<=0:
                            invertedVer[kkm][kkn]=0
                        if invertedVer[kkm][kkn]>=255:
                            invertedVer[kkm][kkn]=255

                #copio la finestra compressa nell'immagine risultante
                xTemp=0
                yTemp=0
                for rTemp in range(indiceXIniziale,indiceXIniziale + f):
                    yTemp=0
                    for cTemp in range(indiceYIniziale,indiceYIniziale + f): 
                        resultMatrix[rTemp,cTemp]=invertedVer[xTemp,yTemp]
                        yTemp= yTemp + 1
                    xTemp= xTemp + 1    

        #Stampa nuovo risultato e salva immagine output
        rgbArray = np.zeros((nrow,ncol,3), 'uint8')
        rgbArray[..., 0] = resultMatrix
        rgbArray[..., 1] = resultMatrix
        rgbArray[..., 2] = resultMatrix

        img = Image.fromarray(rgbArray)
        height, width, channel = rgbArray.shape
        bytesPerLine = 3 * width
          
        qImg = QImage(rgbArray.data, width, height, bytesPerLine, QImage.Format_RGB888)
        qpixel=  QPixmap(qImg)
        winSizeMax = (self.width()-50)/2
        if(width > winSizeMax ):
            qpixel=qpixel.scaledToWidth(winSizeMax)
        self.labelErrore.setText("")
        self.labelImg2.setPixmap(qpixel)    
        plt.figure(2)
        plt.hist(np.array(resultMatrix).flatten(),256,[0,256])
        plt.show()

def GetArrayHistogram(matrix):
     result = np.zeros(256)
     rows,cols= matrix.shape
     c=0
     for x in range (0,rows):
        for y in range(0,cols):
            number= matrix[x][y]
            c+=1
            result[number]=result[number]+1
     return result

if __name__ == '__main__':
    app = QApplication(sys.argv)
    dialog = Dialog()
    sys.exit(dialog.exec_())