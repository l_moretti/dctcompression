import math
import time

import myFunctions
import numpy
import pylab

from scipy.fftpack.realtransforms import dct
from scipy.fftpack.realtransforms import idct
import os


#------ COMPARAZIONE DCT
#INPUT OPZIONALI: due interi a,b tali che a < b.
#INPUT STANDARD: range da 8 a 17
def compareDCTs(*args):
    minRang=8
    maxRang=1600
    skips=25
    if len(args)==2:
        minRang=args[0]
        maxRang=args[1]

    #Array dei risultati
    timeLIB = []
    timeMINE = []
    trend_cubic = []
    trend_log = []
    sizes = []

    
    #Elaborazione delle matrici di dimensione da A a B
    for countTurns in range(minRang,maxRang,skips):
        print("CT: " + str(countTurns))
        singleBlock = numpy.random.randint(255, size=(countTurns,countTurns))
        sizes.append(countTurns)

        print(countTurns)

        #Computazione della libreria
        tLIBs = time.time()
        dct(dct(singleBlock.T, norm='ortho').T, norm='ortho')
        tLIBe = time.time()
        elapsedLIB = tLIBe-tLIBs
        if(elapsedLIB == 0):
            timeLIB.append(0.001)
        else:
            timeLIB.append(elapsedLIB)
        print('lib: ', elapsedLIB)

        #Computazione della DCT fatta da noi
        tMINEs = time.time()
        myHordct = numpy.zeros((countTurns,countTurns))
        myVerdct = numpy.zeros((countTurns,countTurns))
        for jk in range(0,countTurns):
            myHordct[jk]=myFunctions.myDCT(singleBlock[jk])
        myHordct = myHordct.transpose()
        for jk in range(0,countTurns):
            myVerdct[jk]=myFunctions.myDCT(myHordct[jk])
        myVerdct = myVerdct.transpose()
        tMINEe = time.time()
        elapsedMINE = tMINEe - tMINEs
        timeMINE.append(elapsedMINE)
        print('mat: ', elapsedMINE)

        #computo la cubica e la logaritmica
        trend_cubic.append(math.pow(countTurns,3))
        trend_log.append(pow(countTurns,2)*math.log(countTurns))

    #Plotto i grafici con legenda
    pylab.figure(1)
    pylab.title('Elapsed Time')
    pylab.xlabel('Matrix dimension')
    pylab.ylabel('Time (s)')
    pylab.plot(sizes, timeLIB, marker='.', alpha=1, color='b', label="Library DCT")
    pylab.plot(sizes, timeMINE, marker='.', alpha=1, color='r', label='Homemade DCT')
    pylab.plot(sizes, trend_cubic, marker='.', alpha=1, color='g', label="Trend N^3")
    pylab.plot(sizes, trend_log, marker='.', alpha=1, color='y', label="Trend N^2Log(N)") 
    pylab.legend(loc='upper right')
    pylab.yscale('log')
    pylab.show()


compareDCTs(8,409)