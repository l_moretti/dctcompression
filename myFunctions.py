import math
import time

import numpy
import pylab



def myDCT(y):
    N = len(y)
    result = []
    for u in range(0,N):
        somma = 0
        if u==0:
            a=numpy.sqrt(1/N)
        else:
            a=numpy.sqrt(2/N)

        for i in range(0,N):
            somma = somma + y[i]*math.cos(u*math.pi*((2*i+1)/(2*N)))

        result.append(a*somma)
    return result

def myIDCT(y):
    N = len(y)
    c = []
    for i in range(0,N):
        summation = 0

        for u in range(0,N):
            if u==0:
                a=numpy.sqrt(1/N)
            else:
                a=numpy.sqrt(2/N)
            summation = summation + a*y[u]*math.cos(u*math.pi*((2*i+1)/(2*N)))

        c.append(summation)
    return c


y = [231, 32, 233, 161, 24, 71, 140, 245]

print(str(myIDCT(myDCT(y))))